![Ruby](https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Ruby_logo.svg/200px-Ruby_logo.svg.png)
# Ruby

Ruby es un lenguaje interpretado y orientado a objetos  creado por el japones
**yukihiro matz (Matsumoto)** quien empezo a trabajar con el en 1993  y que liberaria en el año 1995.

Algo que distinque a ruby de otros lenguajes es que es un lenguaje preoucupado por el usuario
*(los programadores)* esto hace que su sintaxis sea amigable, ademas de que se toma  enserio la Orientación a Objetos
ya que en Ruby todo es un objeto no existen datos primitivos 





Ruby sigue el principio de la menor sorpresa lo cual quiere decir que quien tenga alguna experiencia en la programacion no se
confunda al leer codigo en Ruby

## Caracteristicas
* Orientado a Objetos.
* Soporta inyección de dependecias.
* Tiene una gran comunidad.
* Manejo de excepciones.
* Altamante portable.
* No tiene soporte completo de unicode  aunque aparentemente lo tiene  para UTF-8

## Simbolos

Los simbolos en ruby son algo de que hablar ya que a diferencia de los *String* que pueden
cambiar  los simbolos no pueden son inmutables ademas de que su definición en ruby es diferente
    
    :nombre-del-simbolo 




