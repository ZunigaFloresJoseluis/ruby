# Sintaxis de Ruby 

Ruby cuenta con una linea de comandos  que ejecutara las instrucciones al igual que 
otras como python, node. Puede arrancar ejecutando:
    
    Linux, Windows : $irb  irb(main):001:0> 

## Ejecutar un archivo de Ruby

Para crear un programa en el lenguaje ruby tiene la extensión .rb y 
pueden ser ejecutados  ya sea en linux o windows como
    
    ruby archivo.rb

## Impresiones en Ruby

Para imprimir en ruby puede ejecutar en un archivo rb o en la consala 
    
    Con la instrucción  > puts "Hola mundo!"
    
    Con la instrucción  > print("Hola mundo")
    

## Cadenas 

Ruby maneja cadenas de dos formas como comillas simples **''** o comillas  dobles **""** 
, estas dos formas se diferencian  en que en las comillas simples no se respetan los espacion o saltos de lineas *\n*
entre otros simbolos por otra parte las cadenas doble si las aceptan y las traducen correctamemte.

Se puede hacer operaciones como concatenación de una manera mas intutiva tal como.

    ruby > "bar"+"foo" => "barfoo"
    ruby > "a"*2 => "aa"
    ruby > word = "hola"; word[0] => "h"
    
    Asi como tambien se puede realizar una subcadena  con esta ultima expressión 
    ruby word[0,2]=> "ho"

## Expressiones 

Pendiente
